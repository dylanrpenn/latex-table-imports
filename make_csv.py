"""Save a csv file."""
# Imports
from pathlib import Path

import pandas as pd

# Make and save simple DataFrame
d = {"colA": [1, 2], "colB": [3, 4]}
df = pd.DataFrame(d)
print(f"{df=}")

fpath = Path(__file__).parent.joinpath("file1").with_suffix(".csv")
df.to_csv(fpath)

# ,col1,col2
# 0,1,3
# 1,2,4

# Now with columns with digits and special characters
d = {"col1": [1, 2], "col_B": [3, 4]}
df = pd.DataFrame(d)
print(f"{df=}")

fpath = Path(__file__).parent.joinpath("file2").with_suffix(".csv")
df.to_csv(fpath)

# ,col1,col_B
# 0,1,3
# 1,2,4

# Now with commas in the data
d = {"colA": [[1, 2], [3, 4]], "colB": [3, 4]}
df = pd.DataFrame(d)
print(f"{df=}")

fpath = Path(__file__).parent.joinpath("file3").with_suffix(".csv")
df.to_csv(fpath, sep=";")

# ;colA;colB
# 0;[1, 2];3
# 1;[3, 4];4
