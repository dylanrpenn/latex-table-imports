# LaTeX Table Imports

Use `make_csv.py` to generate .csv files.
Use `latex_tables.tex` to read the .csv files and generate a .pdf with tables.